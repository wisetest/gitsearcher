import { Pipe, PipeTransform } from '@angular/core';
import { SearcherComponent } from '../components/searcher/searcher.component'

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(value: any[], args?: any): any {
    const result = [];
    const map = new Map();
    for (const item of value) {
      if (!map.has(item.language)) {
        map.set(item.language, true);
        result.push({ language: item.language });
      }
    }
    return result;
  }
}
