import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearcherComponent } from '../components/searcher/searcher.component';
import { FavouriteComponent } from '../components/favourite/favourite.component';



const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'main' },
  { path: 'main', component: SearcherComponent },
  { path: 'favourite', component: FavouriteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
